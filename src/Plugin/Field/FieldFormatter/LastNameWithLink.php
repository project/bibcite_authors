<?php

namespace Drupal\bibcite_authors\Plugin\Field\FieldFormatter;

use Drupal\bibcite_entity\Entity\Contributor;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'Authors (with link to user)' formatter.
 *
 * @FieldFormatter(
 *   id = "bibcite_authors_last_name_with_link",
 *   label = @Translation("Authors (Last name first with link to user)"),
 *   field_types = {
 *     "bibcite_contributor"
 *   }
 * )
 */
class LastNameWithLink extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $author_id = $item->target_id;
      $user = \Drupal::entityQuery('user')
        ->accessCheck(FALSE)
        ->condition('field_author', $author_id)
        ->execute();
      $author = Contributor::load($author_id);
      $last = trim($author->last_name->getString());
      $first = trim($author->first_name->getString());
      $middle = $author->middle_name->getString() ? ' ' . trim($author->middle_name->getString()) : NULL;
      if (empty($user)) {
        $element[$delta] = [
          '#markup' => '<p>' . $last . ', ' . $first . $middle . '</p>',
        ];
      }
      else {
        $element[$delta] = [
          '#markup' => '<a href="/user/' . reset($user) . '">' . $last . ', ' . $first . $middle . "</a>",
        ];
      }
    }

    return $element;
  }

}
