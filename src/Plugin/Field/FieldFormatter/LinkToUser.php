<?php

namespace Drupal\bibcite_authors\Plugin\Field\FieldFormatter;

use Drupal\bibcite_entity\Entity\Contributor;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\user\Entity\User;

/**
 * Plugin implementation of the 'Authors (with link to user)' formatter.
 *
 * @FieldFormatter(
 *   id = "bibcite_authors_authors_with_link_to_user",
 *   label = @Translation("Authors (with link to user)"),
 *   field_types = {
 *     "bibcite_contributor"
 *   }
 * )
 */
class LinkToUser extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $author_id = $item->target_id;
      $user = \Drupal::entityQuery('user')
        ->accessCheck(FALSE)
        ->condition('field_author', $author_id)
        ->execute();
      if (empty($user)) {
        $author = Contributor::load($author_id);
        $first = isset($author->first_name) ? trim($author->first_name->getString()) . ' ' : NULL;
        $middle = isset($author->middle_name) ? trim($author->middle_name->getString()) . ' ' : NULL;
        $last = isset($author->last_name) ? trim($author->last_name->getString()) : NULL;
        if (!empty($author)) {
          $element[$delta] = [
            '#markup' => '<p>' . $first . $middle . $last . '</p>',
          ];
        };
      }
      else {
        $author = User::load(reset($user));
        $element[$delta] = [
          '#markup' => '<a href="/user/' . $author->id() . '">' . $author->field_name->getString() . "</a>",
        ];
      }
    }

    return $element;
  }

}
